FROM adoptopenjdk:13-jre-hotspot
COPY target/*.jar app.jar
COPY wait-for-it.sh wait-for-it.sh
RUN chmod +x wait-for-it.sh