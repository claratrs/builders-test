package dev.claratersi.builderstest.domain.client;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.time.LocalDate;
import java.time.Period;

public class ClientMapperTest {

    private Client clientExpected;
    private ClientDTO clientDTOExpected;

    @BeforeEach
    public void setUp() {
        clientExpected = Client.builder()
                .id(1L)
                .name("Clara")
                .cpf("66263829265")
                .dateOfBirth(LocalDate.parse("1989-05-13"))
                .build();
        clientDTOExpected = ClientDTO.builder()
                .id(1L)
                .name("Clara")
                .cpf("66263829265")
                .dateOfBirth(LocalDate.parse("1989-05-13"))
                .age(Period.between(LocalDate.parse("1989-05-13"), LocalDate.now()).getYears())
                .build();
    }

    @Test
    public void shouldReturnAClientEntity() {
        Client result = ClientMapper.toEntity(clientDTOExpected);
        Assertions.assertEquals(clientExpected, result);
    }

    @Test
    public void shouldReturnAClientDTO() {
        ClientDTO result = ClientMapper.toDTO(clientExpected);
        Assertions.assertEquals(clientDTOExpected, result);
    }

    @Test
    public void shouldReturnZeroAsAgeIfDateOfBirthNotSet() {
        Client clientWithoutDateOfBirth = Client.builder().id(1L).name("Clara").cpf("66263829265").build();
        ClientDTO result = ClientMapper.toDTO(clientWithoutDateOfBirth);
        Assertions.assertEquals(0, result.getAge());
    }
}
