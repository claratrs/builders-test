package dev.claratersi.builderstest.service;

import dev.claratersi.builderstest.domain.client.Client;
import dev.claratersi.builderstest.domain.client.ClientDTO;
import dev.claratersi.builderstest.exceptions.NotFoundException;
import dev.claratersi.builderstest.repository.ClientRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {

    @Mock
    ClientRepository mockedClientRepository;

    @InjectMocks
    ClientService classUnderTest;


    @Test
    public void shouldCallSaveAndReturnClientId() {
        Client client = Client.builder()
                .id(1L).name("Clara").cpf("66263829265")
                .dateOfBirth(LocalDate.parse("1989-05-13")).build();
        ClientDTO clientDTO = ClientDTO.builder()
                .id(1L).name("Clara").cpf("66263829265").dateOfBirth(LocalDate.parse("1989-05-13"))
                .age(Period.between(LocalDate.parse("1989-05-13"), LocalDate.now()).getYears())
                .build();

        Mockito.when(this.mockedClientRepository.save(client)).thenReturn(client);

        Long result = this.classUnderTest.addClient(clientDTO);

        Mockito.verify(this.mockedClientRepository, Mockito.times(1)).save(client);
        Assertions.assertEquals(1L, result);
    }

    @Test
    public void shouldReturnMapWithPaginatedResults() {
        Client client1 = Client.builder().id(1L).name("Clara").cpf("66263829265")
                .dateOfBirth(LocalDate.parse("1989-05-13")).build();
        Client client2 = Client.builder().id(2L).name("Ivan").cpf("94080951035")
                .dateOfBirth(LocalDate.parse("1989-05-13")).build();
        List<Client> clients = new ArrayList<>();
        clients.add(client1);
        clients.add(client2);
        Page<Client> page = new PageImpl<>(clients);

        Mockito.when(this.mockedClientRepository.findAll(Mockito.any(), Mockito.any(Pageable.class))).thenReturn(page);

        Map<String, Object> result = this.classUnderTest.find(null, null, 0, 2);

        Mockito.verify(this.mockedClientRepository, Mockito.times(1))
                .findAll(Mockito.any(), Mockito.any(Pageable.class));

        Assertions.assertEquals(0, result.get("currentPage"));
        Assertions.assertEquals(1, result.get("pages"));
        Assertions.assertEquals(2L, result.get("total"));
        Assertions.assertNotNull(result.get("data"));
    }

    @Test
    public void shouldThrowExceptionIfClientNotFound() throws NotFoundException {
        Mockito.when(this.mockedClientRepository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(NotFoundException.class, () -> this.classUnderTest.findById(1L));
    }

    @Test
    public void shouldReturnClientDTO() throws NotFoundException {
        Client client = Client.builder().id(1L).name("Clara").cpf("66263829265")
                .dateOfBirth(LocalDate.parse("1989-05-13")).build();
        Mockito.when(this.mockedClientRepository.findById(1L)).thenReturn(Optional.of(client));

        ClientDTO result = this.classUnderTest.findById(1L);

        ClientDTO expected = ClientDTO.builder()
                .id(1L).name("Clara").cpf("66263829265").dateOfBirth(LocalDate.parse("1989-05-13"))
                .age(Period.between(LocalDate.parse("1989-05-13"), LocalDate.now()).getYears()).build();
        Assertions.assertEquals(expected, result);
    }

}
