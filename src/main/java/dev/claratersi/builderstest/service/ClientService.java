package dev.claratersi.builderstest.service;

import dev.claratersi.builderstest.domain.client.Client;
import dev.claratersi.builderstest.domain.client.ClientDTO;
import dev.claratersi.builderstest.domain.client.ClientMapper;
import dev.claratersi.builderstest.exceptions.NotFoundException;
import dev.claratersi.builderstest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Long addClient(ClientDTO clientDTO) {
        Client client = this.clientRepository.save(ClientMapper.toEntity(clientDTO));
        return client.getId();
    }

    public Map<String, Object> find(String name, String cpf, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Client example = Client.builder().name(name).cpf(cpf).build();
        Example<Client> query = Example.of(example, ExampleMatcher.matchingAll().withIgnoreCase());

        Page<Client> clientsPage = this.clientRepository.findAll(query, pageable);
        List<ClientDTO> clients = ClientMapper.manyToDTO(clientsPage.getContent());

        Map<String, Object> result = new HashMap<>();
        result.put("data", clients);
        result.put("currentPage", clientsPage.getNumber());
        result.put("total", clientsPage.getTotalElements());
        result.put("pages", clientsPage.getTotalPages());
        return result;
    }

    public ClientDTO findById(Long id) throws NotFoundException {
        Optional<Client> client = this.clientRepository.findById(id);
        if (client.isEmpty()) {
            throw new NotFoundException("Client not found");
        }
        return ClientMapper.toDTO(client.get());
    }

    public void update(Long id, ClientDTO clientDTO) throws NotFoundException {
        Optional<Client> client = this.clientRepository.findById(id);

        if (client.isEmpty()) {
            throw new NotFoundException("Client not found");
        }

        Client clientToUpdate = ClientMapper.toEntity(clientDTO);
        clientToUpdate.setId(id);

        this.clientRepository.save(clientToUpdate);
    }

    public void patch(Long id, ClientDTO clientDTO) throws NotFoundException {
        Optional<Client> client = this.clientRepository.findById(id);

        if (client.isEmpty()) {
            throw new NotFoundException("Client not found");
        }

        Client clientToUpdate = Client.builder()
                .id(id)
                .name(Optional.ofNullable(clientDTO.getName()).orElse(client.get().getName()))
                .cpf(Optional.ofNullable(clientDTO.getCpf()).orElse(client.get().getCpf()))
                .dateOfBirth(Optional.ofNullable(clientDTO.getDateOfBirth()).orElse(client.get().getDateOfBirth()))
                .build();

        this.clientRepository.save(clientToUpdate);
    }

    public void delete(Long id) throws NotFoundException {
        Optional<Client> client = this.clientRepository.findById(id);

        if (client.isEmpty()) {
            throw new NotFoundException("Client not found");
        }

        this.clientRepository.deleteById(id);
    }

}
