package dev.claratersi.builderstest.domain.client;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClientMapper {

    public static Client toEntity(ClientDTO clientDTO) {
        return Client.builder()
                .id(clientDTO.getId())
                .name(clientDTO.getName())
                .cpf(clientDTO.getCpf())
                .dateOfBirth(clientDTO.getDateOfBirth())
                .build();
    }

    public static ClientDTO toDTO(Client client) {
        return ClientDTO.builder()
                .id(client.getId())
                .name(client.getName())
                .cpf(client.getCpf())
                .dateOfBirth(client.getDateOfBirth())
                .age(ClientMapper.calculateAge(client.getDateOfBirth()))
                .build();
    }

    public static List<ClientDTO> manyToDTO(List<Client> clients) {
        return clients.stream().map(ClientMapper::toDTO).collect(Collectors.toList());
    }

    private static int calculateAge(LocalDate dateOfBirth) {
        if (dateOfBirth == null) return 0;
        return Period.between(dateOfBirth, LocalDate.now()).getYears();
    }

}
