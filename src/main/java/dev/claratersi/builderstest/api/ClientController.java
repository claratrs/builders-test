package dev.claratersi.builderstest.api;

import dev.claratersi.builderstest.domain.client.ClientDTO;
import dev.claratersi.builderstest.exceptions.NotFoundException;
import dev.claratersi.builderstest.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Map;

@Slf4j
@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/clients")
    public ResponseEntity<Object> add(@RequestBody ClientDTO clientDTO) {
        try {
            Long clientAddedId = this.clientService.addClient(clientDTO);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(clientAddedId)
                    .toUri();
            return ResponseEntity.created(location).build();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping("/clients")
    public ResponseEntity<Map<String, Object>> find(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String cpf,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "2") int size
    ) {
        try {
            return ResponseEntity.ok(this.clientService.find(name, cpf, page, size));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping("/clients/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(this.clientService.findById(id));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @PutMapping("/clients/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody ClientDTO clientDTO) {
        try {
            this.clientService.update(id, clientDTO);
            return ResponseEntity.noContent().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @PatchMapping("/clients/{id}")
    public ResponseEntity<Object> patch(@PathVariable Long id, @RequestBody ClientDTO clientDTO) {
        try {
            this.clientService.patch(id, clientDTO);
            return ResponseEntity.noContent().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @DeleteMapping("/clients/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        try {
            this.clientService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

}
