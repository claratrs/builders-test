package dev.claratersi.builderstest.repository;

import dev.claratersi.builderstest.domain.client.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
