# Builders Test App

## Instruções para rodar o projeto

Na raiz do projeto existe um arquivo `docker-compose.yml` que irá baixar as imagens da aplicação e do banco de dados. 
Substitua dos dados de mapeamentos e usuário/senha e rode o comando `docker-compose up -d` para criar e iniciar os containers.

## Instruções para build

Caso queira gerar o arquivo jar e construir a imagem do docker, execute na raiz o projeto o comando `./mvnw clean package -DskipTests`, e depois `docker build -t <nome-da-imagem>:<tag> .`.